enum ObstacleType {
	obs_rec,
};

struct Obstacle {
	int type;
	int moving;
	int kills;
	int rotating;
	Color color;
	float rotation;
	float x, y, width, height;
	float mov_x, mov_y;
	float scale;
	int tex_index;
	char *texture_path;
	Texture2D texture;
};
