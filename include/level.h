struct Level {
	int obs_len;
	struct Obstacle *obstacles;
	Color primary_color;
	Color background_color;
	int textures_len;
	char **textures;
};

struct Level *load_level(char *path) {
	struct Level *level = malloc(sizeof(struct Level));
	void *file = fopen(path, "r");
	if (!file) {
		printf("\n\nfile doesn't exist\n");
		int *bye = 0;
		int m = *bye;
	}
	int len = 0;
	fread(&len, sizeof(int), 1, file);
	level->obs_len = len;
	level->obstacles = malloc(len * sizeof(struct Obstacle));
	fread(level->obstacles, sizeof(struct Obstacle), len, file);
	fread(&len, sizeof(int), 1, file);
	int string_len;
	char **textures = malloc(len * sizeof(char*));
	for (int i = 0; i < len; i++) {
		fread(&string_len, sizeof(int), 1, file);
		textures[i] = malloc(string_len) + 1;
		fread(textures[i], string_len, 1, file);
		textures[i][string_len] = 0;
	}
	level->textures_len = len;
	level->textures = textures;
	fclose(file);
	for (int i = 0; i < level->textures_len; i++) {
		printf("%s\n", textures[i]);
	}
	return level;
}

