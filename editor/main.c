#include <raylib.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define EDITOR
#include "obstacle.h"
#include "level.h"
#define RAYGUI_IMPLEMENTATION
#include "../include/raygui.h"

enum Operation {
	op_nothing,
	op_resize,
	op_drag,
	op_move,
	op_set_moving
};

const float unit_scale = /* one */ 10/*th*/;

int check_button(int x, int y, int width, int height) {
	Vector2 mouse_pos = GetMousePosition();
	if (mouse_pos.x > x &&
			mouse_pos.x < x + width &&
			mouse_pos.y > y &&
			mouse_pos.y < y + height) {
		return 1;
	}
	return 0;
}

int draw_button(char *text, int x, int y) {
	DrawRectangleLinesEx((Rectangle){ x, y, 11 * strlen(text) + 20, 30}, 5, BLACK);
	DrawRectangle(x + 5, y + 5, 11 * strlen(text) + 10, 20, LIGHTGRAY);
	DrawText(text, x + 10, y + 5, 20, BLACK);
}

void make_texture_list(struct Level *level) {
	char **textures = malloc(20 * sizeof(char*));
	int len = 0;
	int max = 20;
	int found = 0;
	for (int i = 0; i < level->obs_len; i++) {
		if (!level->obstacles[i].texture_path) {
			continue;
		}
		if (len == max - 1) {
			max += 20;
			textures = realloc(textures, max * sizeof(char*));
		}
		for (int x = 0; x < len; x++) {
			if (strlen(textures[x]) == strlen(level->obstacles[i].texture_path)) {
				if (!memcmp(textures[x], level->obstacles[i].texture_path, strlen(textures[x]))) {
					found = 1;
					break;
				}
			}
		}
		if (found) {
			found = 0;
			continue;
		}
		textures[len] = level->obstacles[i].texture_path;
		len++;
	}
	level->textures = textures;
	level->textures_len = len;
}

void save_level(struct Level *level, char *path) {
	void *file = fopen(path, "w");
	int len = 0;
	for (int i = 0; i < level->obs_len; i ++) {
		if (level->obstacles[i].type != -1) {
			len++;
		}
	}
	printf("%i\n", len);
	fwrite(&len, sizeof(int), 1, file);
	for (int i = 0; i < level->obs_len; i ++) {
		if (level->obstacles[i].type == -1) {
			fwrite(&level->obstacles[i], sizeof(struct Obstacle), 1, file);
		}
	}
	fwrite(level->obstacles, sizeof(struct Obstacle), level->obs_len, file);
	fwrite(&level->textures_len, sizeof(int), 1, file);
	printf("%i\n", level->textures_len);
	int size = 0;
	for (int i = 0; i < level->textures_len; i++) {
		int test = strlen(level->textures[i]);
		fwrite(&test, sizeof(int), 1, file);
		fwrite(level->textures[i], test, 1, file);
	}
	fclose(file);
}

int main() {
	InitWindow(640, 480, "editor");
	SetWindowState(FLAG_WINDOW_RESIZABLE);
	SetTargetFPS(60);
	int winh = GetScreenHeight();
	int winw = GetScreenWidth();

	struct Level *level = malloc(sizeof(struct Level));
	Color fg = BLACK;
	Color bg = RAYWHITE;
	int obs_len = 0;
	int obs_size = 10;
	struct Obstacle *obstacles = malloc(obs_size * sizeof(struct Obstacle));
	struct Obstacle cur;
	int selected = -1;
	int op = 0;
	float level_x = 0;
	float level_x_max = 10000;
	int found = 1;
	int texture = 0;
	int save = 0;
	int load = 0;
	Vector2 lastpos;
	char *text = calloc(1, 101);
	Vector2 mouse_pos;

	float ratio = 1;
	float yscale = winh / unit_scale;
	float xscale = winw / unit_scale;


	while (!WindowShouldClose()) {
		Vector2 mouse_pos = GetMousePosition();
		winh = GetScreenHeight();
		winw = GetScreenWidth();
		ratio = (float)winh / winw;
		yscale = winh / unit_scale;
	    xscale = winw / unit_scale * ratio;
		mouse_pos.y /= yscale;
		mouse_pos.y = 10. - mouse_pos.y;
		mouse_pos.x /= xscale;
		if (!texture && !save && !load) {
			if (IsKeyPressed(KEY_A)) {
				if (level_x < 10.) {
					level_x = 0;
				} else {
					level_x -= 10.;
				}
			}
			if (IsKeyPressed(KEY_D)) {
				level_x += 10.;
			}
			if (IsKeyPressed(KEY_DELETE)) {
				memset(&obstacles[selected], 0, sizeof(struct Obstacle));
				obstacles[selected].type = -1;
			}
			if (op == op_drag) {
				if (IsMouseButtonDown(0)) {
					cur = obstacles[selected];
					cur.x = floor((mouse_pos.x - lastpos.x) * 10) / 10 + level_x;
					cur.y = floor((mouse_pos.y - lastpos.y) * 10) / 10;
					if (cur.x < 0) {
						cur.x = 0;
					}
					if (cur.texture_path) {
						float texratio = 0;
						texratio = cur.texture.height / cur.texture.width;
						if (cur.y - cur.scale * texratio < 1) {
							cur.y = 1 + cur.scale * texratio;
						}
					} else {
						if (cur.y - cur.height < 1) {
							cur.y = 1 + cur.height;
						}
					}
					obstacles[selected] = cur;
				} else {
					op = 0;
				}
			} else if (op == op_move) {
				if (IsMouseButtonDown(0)) {
					level_x = (float)(mouse_pos.x) * level_x_max;
					if (level_x < 0)
						level_x = 0;
					if (level_x > level_x_max)
						level_x = level_x_max;
				} else {
					op = 0;
				}
			} else if (op == op_resize) {
				if (IsMouseButtonDown(1)) {
					cur = obstacles[selected];
					if (!cur.texture_path) {
						if (mouse_pos.x - (cur.x - level_x) > 0.5) {
							cur.width = floor((mouse_pos.x - (cur.x - level_x)) * 10) / 10;
						} else {
							cur.width = 0.5;
						}
						if (cur.y - mouse_pos.y > 0.5) {
							cur.height = floor((cur.y - mouse_pos.y) * 10) / 10;
						} else {
							cur.height = 0.5;
						}
						if (cur.y - cur.height < 1) {
							cur.height = cur.y - 1;
						}
						printf("%f\n", cur.height);
					} else {
						float texratio = 0;
						texratio = cur.texture.height / cur.texture.width;
						if ((mouse_pos.x - (cur.x - level_x)) * texratio > (cur.y - mouse_pos.y)) {
							cur.scale = floor((mouse_pos.x - (cur.x - level_x)) * 10) / 10;
						} else {
							cur.scale = floor(((cur.y - mouse_pos.y) / texratio) * 10) / 10;
						}
						cur.width = cur.texture.width / xscale * cur.scale;
						cur.height = cur.texture.height / yscale * cur.scale;
					}
					obstacles[selected] = cur;
				} else {
					op = 0;
				}
			} else {
				if (IsMouseButtonPressed(1)) {
					for (int i = 0; i < obs_len; i++) {
						cur = obstacles[i];
						if (cur.texture_path) {
							float texratio = 0;
							texratio = cur.texture.height / cur.texture.width;
							if (mouse_pos.x > cur.x - level_x &&
									mouse_pos.x < cur.x + cur.scale - level_x &&
									mouse_pos.y < cur.y &&
									mouse_pos.y > cur.y - cur.scale * texratio) {
								op = op_resize;
								selected = i;
								break;
							}
						} else {
							if (mouse_pos.x > cur.x - level_x &&
									mouse_pos.x < cur.x + cur.width - level_x &&
									mouse_pos.y < cur.y &&
									mouse_pos.y > cur.y - cur.height) {
								op = op_resize;
								selected = i;
								break;
							}
						}
					}
				}
				if (IsMouseButtonPressed(0)) {
					found = 1;
						if (mouse_pos.x * xscale > winw / 5 &&
								mouse_pos.x * xscale < winw / 5 * 4 &&
								winh - mouse_pos.y * yscale > 10 &&
								winh - mouse_pos.y * yscale < 20) {
							op = op_move;
						}
						// moving
						if (check_button(5, 5, 20, 20)) {
							if (obstacles[selected].kills) {
								obstacles[selected].kills = 0;
							} else {
								obstacles[selected].kills = 1;
							}
							printf("selected\n");
						} else {
							found = 0;
						}
						if (!found) {
							for (int i = 0; i < obs_len; i++) {
								cur = obstacles[i];
								if (cur.texture_path) {
									printf("test\n");
									float texratio = 0;
									texratio = cur.texture.height / cur.texture.width;
									if (mouse_pos.x > cur.x - level_x &&
											mouse_pos.x < cur.x + cur.scale - level_x &&
											mouse_pos.y < cur.y &&
											mouse_pos.y > cur.y - cur.scale * texratio) {
										printf("testi\n");
										op = op_drag;
										lastpos.x = mouse_pos.x - cur.x + level_x;
										lastpos.y = mouse_pos.y - cur.y;
										found = 1;
										selected = i;
										break;
									}
								} else {
									if (mouse_pos.x > cur.x - level_x &&
											mouse_pos.x < cur.x + cur.width - level_x &&
											mouse_pos.y < cur.y &&
											mouse_pos.y > cur.y - cur.height) {
										printf("testii\n");
										op = op_drag;
										lastpos.x = mouse_pos.x - cur.x + level_x;
										lastpos.y = mouse_pos.y - cur.y;
										found = 1;
										selected = i;
										break;
									}
								}
							}
						}
				}
				if (IsMouseButtonPressed(2)) {
					obstacles[obs_len] = (struct Obstacle){ .x = mouse_pos.x - 1 + level_x, .y = mouse_pos.y + 1, .width = 1, .height = 1, .color = BLACK };
					printf("test\n");
					if (obs_len == obs_size-1) {
						obs_size += 10;
						obstacles = realloc(obstacles, obs_size * sizeof(struct Obstacle));
					}
					selected = obs_len;
					obs_len++;
				}
			}
		}
		BeginDrawing();
		ClearBackground(bg);
		DrawRectangleGradientV(0, 0, winw, winh, YELLOW, BLUE);
		// obstacles
		for (int i = 0; i < obs_len; i++) {
			cur = obstacles[i];
			if (cur.x + cur.width < level_x || cur.x > level_x + winw) {
				continue;
			}
			if (cur.texture_path) {
				float texratio = 0;
				texratio = cur.texture.height / cur.texture.width;
				DrawTextureEx(cur.texture, (Vector2){ (cur.x - level_x) * xscale, (10. - cur.y) * yscale}, 0, cur.scale * xscale / cur.texture.width, WHITE);
			} else {
				DrawRectangle((cur.x - level_x) * xscale, (10. - cur.y) * yscale, cur.width * xscale, cur.height * yscale + 1, cur.color);
			}
		}
		// selection
		if (selected != -1) {
			cur = obstacles[selected];
			if (cur.texture_path) {
				float texratio = 0;
				texratio = cur.texture.height / cur.texture.width;
				DrawRectangleLinesEx((Rectangle){(cur.x - level_x) * xscale - 5, (10. - cur.y) * yscale - 5, xscale * cur.scale + 10, yscale * cur.scale * texratio + 10}, 5, GRAY);
			} else {
				DrawRectangleLinesEx((Rectangle){(cur.x - level_x) * xscale - 5, (10. - cur.y) * yscale - 5, obstacles[selected].width * xscale + 10, obstacles[selected].height * yscale + 10}, 5, GRAY);
			}
		}
		// progress bar
		DrawRectangleLinesEx((Rectangle){winw / 5 - 5, 5, winw / 5 * 3 + 10, 20}, 5, GRAY);
		DrawRectangle(winw / 5, 10, (float)level_x / level_x_max * winw / 5 * 3, 10, BLACK);
		// buttons
		{
			// kills
			DrawRectangleLinesEx((Rectangle){5, 5, 20, 20}, 5, GRAY);
			if (obstacles[selected].kills) {
				DrawRectangle(10, 10, 10, 10, BLACK);
			}
			DrawText("kills", 30, 5, 20, BLACK);
			if (GuiButton((Rectangle){ 5, 30, 100, 20 }, "texture")) {
				if (texture) {
					texture = 0;
				} else {
					texture = 1;
					if (obstacles[selected].texture_path) {
						strcpy(text, obstacles[selected].texture_path);
					}
				}
			}
			if (GuiButton((Rectangle){ winw - 105, 5, 100, 20 }, "save")) {
				if (save) {
					save = 0;
				} else {
					save = 1;
					memset(text, 0, strlen(text));
				}
			}
			if (save) {
				if (GuiTextBox((Rectangle){ winw / 4, winh / 2 - 15, winw / 2, 30 }, text, 100, 1)) {
					level->primary_color = fg;
					level->background_color = bg;
					level->obs_len = obs_len;
					level->obstacles = obstacles;
					make_texture_list(level);
					for (int i = 0; i < level->textures_len; i++) {
						printf("%s\n", level->textures[i]);
					}
					save_level(level, text);
					save = 0;
				}
			}
			if (texture) {
				if (GuiTextBox((Rectangle){ winw / 4, winh / 2 - 15, winw / 2, 30 }, text, 100, 1)) {
					obstacles[selected].texture_path = malloc(strlen(text) + 1);
					sprintf(obstacles[selected].texture_path, "%s", text);
					obstacles[selected].texture = LoadTexture(obstacles[selected].texture_path);
					memset(text, 0, strlen(text));
					if (obstacles[selected].texture.width == 0 || obstacles[selected].texture.height == 0) {
						obstacles[selected].texture_path = (int)0;
						texture = 0;
						continue;
					}
					obstacles[selected].scale = 1;
					obstacles[selected].width = obstacles[selected].texture.width;
					obstacles[selected].height = obstacles[selected].texture.height;
					texture = 0;
				}
			}
		}
		// ground
		DrawRectangle(0, 9. * yscale, winw, 1. * yscale, BLACK);
		EndDrawing();
	}

	CloseWindow();
}
