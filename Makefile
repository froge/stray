all:
	gcc src/main.c -o stray -Iinclude -lraylib
	gcc editor/main.c -o steditor -Iinclude -lraylib -lm
run: all
	./steditor

win:
	x86_64-w64-mingw32-gcc editor/main.c -L./ -static -lraylib -lopengl32 -lgdi32 -lwinmm -lpthread -Iinclude/ -o steditor.exe
	x86_64-w64-mingw32-gcc src/main.c -L./ -static -lraylib -lopengl32 -lgdi32 -lwinmm -lpthread -Iinclude/ -o stray.exe
