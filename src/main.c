#include <raylib.h>
#include <stdlib.h>
#include <stdio.h>
#include "obstacle.h"
#include "level.h"

const float player_width = 1.;
const float player_height = .5;
const float unit_scale = /* one */ 10/*th*/;

struct Player {
	Vector2 pos;
	Vector2 velocity;
};

int main() {
	InitWindow(680, 450, "stray");
	SetWindowState(FLAG_WINDOW_RESIZABLE);
	SetTargetFPS(60);

	struct Player *player = malloc(sizeof(struct Player));
	player->pos.x = 0;
	player->pos.y = 10;
	player->velocity.x = 0;
	player->velocity.y = -1;

	float delta_time = 0;
	float ratio = 1;
	int winh = GetScreenHeight();
	int winw = GetScreenWidth();
	float yscale = winh / unit_scale;
	float xscale = winw / unit_scale;
	float level_x = 0;

	struct Level *level = load_level("test.lvl");
	struct Obstacle *obstacles = level->obstacles;
	struct Obstacle cur;
	int obs_len = level->obs_len;

	while (!WindowShouldClose()) {
		winh = GetScreenHeight();
		winw = GetScreenWidth();
		ratio = (float)winh / winw;
		yscale = winh / unit_scale;
		xscale = winw / unit_scale * ratio;
		delta_time = GetFrameTime();
		if (IsKeyDown(KEY_A)) player->velocity.x = -5.4;
		if (IsKeyDown(KEY_D)) player->velocity.x = 5.4;
		if (IsKeyPressed(KEY_SPACE)) player->velocity.y = 6;
		//printf("%f, %f, %f\n", player->velocity.y, player->pos.y, ratio);
		if (player->velocity.x > 0 && !IsKeyDown(KEY_D)) {
			if (player->velocity.x < 5.) {
				player->velocity.x = 0;
			} else {
				player->velocity.x -= 5. * delta_time;
			}
		} else if (player->velocity.x < 0 && !IsKeyDown(KEY_A)) {
			if (player->velocity.x > -5.) {
				player->velocity.x = 0;
			} else {
				player->velocity.x += 5. * delta_time;
			}
		}


		// collision checking
		//
		//
		if (player->pos.y + player->velocity.y * delta_time < 1.5) {
			player->pos.y = 1.5;
			player->velocity.y = 0;
		} else {
			int col = 0;
			for (int i = 0; i < obs_len; i++) {
				cur = obstacles[i];
				if (CheckCollisionRecs((Rectangle){player->pos.x, 10. - (player->pos.y - player_height + 0.1 + player->velocity.y * delta_time), player_width, 0.1},
							(Rectangle){cur.x, 10. - cur.y, cur.width, cur.height})) {
					player->pos.y = cur.y + player_height;
					player->velocity.y = 0;
					col = 1;
				}
				if (CheckCollisionRecs((Rectangle){player->pos.x, 10. - (player->pos.y + player->velocity.y * delta_time), player_width, 0.1},
							(Rectangle){cur.x, 10. - cur.y, cur.width, cur.height})) {
					player->pos.y = cur.y - cur.height;
					player->velocity.y = 0;
					col = 1;
				}
			}
			if (!col) {
				player->pos.y += player->velocity.y * delta_time;
				player->velocity.y -= 9.81 * delta_time;
			}
		}
		if (player->pos.x + player->velocity.x * delta_time < 0) {
			player->pos.x = 0;
		} else {
			int col = 0;
			for (int i = 0; i < obs_len; i++) {
				cur = obstacles[i];
				if (CheckCollisionRecs((Rectangle){player->pos.x + player_width - 0.1 + player->velocity.x * delta_time, 10. - (player->pos.y), 0.1, player_height},
							(Rectangle){cur.x, 10. - cur.y, cur.width, cur.height})) {
					printf("hello\n");
					player->pos.x = cur.x - player_width;
					player->velocity.x = 0;
					col = 1;
				}
				if (CheckCollisionRecs((Rectangle){player->pos.x + player->velocity.x * delta_time, 10. - (player->pos.y), 0.1, player_height},
							(Rectangle){cur.x, 10. - cur.y, cur.width, cur.height})) {
					//printf("hello\n");
					player->pos.x = cur.x + cur.width;
					player->velocity.x = 0;
					col = 1;
				}
			}
			if (!col) {
				player->pos.x += player->velocity.x * delta_time;
			}
		}
		if (player->velocity.x == -0) {
			player->velocity.x = 0;
		}
		//
		//
		//collision checking end

		level_x = player->pos.x - 4;

		BeginDrawing();
		ClearBackground(RAYWHITE);
		for (int i = 0; i < obs_len; i++) {
			cur = obstacles[i];
			if (cur.x + cur.width < level_x || cur.x > level_x + winw) {
				continue;
			}
			if (cur.texture_path) {
				float texratio = 0;
				texratio = cur.texture.height / cur.texture.width;
				DrawTextureEx(cur.texture, (Vector2){ (cur.x - level_x) * xscale, (10. - cur.y) * yscale}, 0, cur.scale * xscale / cur.texture.width, WHITE);
			} else {
				DrawRectangle((cur.x - level_x) * xscale, (10. - cur.y) * yscale, cur.width * xscale, cur.height * yscale + 1, cur.color);
			}
		}
		DrawRectangle(4 * xscale, GetScreenHeight() - player->pos.y * yscale, player_width * xscale, player_height * yscale, BLACK);
		DrawRectangle(0, 9. * yscale, winw, 1. * yscale, BLACK);
		EndDrawing();
	}

	CloseWindow();
}
